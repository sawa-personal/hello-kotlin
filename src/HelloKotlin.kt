//*******************************************************
// HelloKotlin.kt
// created by Sawada Tatsuki on 2018/03/12.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* KotlinでHelloWorld */

import java.util.*;

class HelloKotlin (s: String){
    var string = s //表示する文字

    //stringに格納された文字列を表示
    fun greet() {
        println("$string")
    }
}

//メインクラス
fun main(args: Array<String>) {
    val hello = HelloKotlin("Hello Kotlin!") //挨拶用オブジェクトを生成
    hello.greet() //挨拶する
    hello.string = "Goodnight Kotlin!"
    hello.greet()
}